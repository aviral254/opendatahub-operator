
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="customizing-jupyter-images"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Customizing Jupyter Images
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

AICoE-JupyterHub provides a set of commonly used Jupyter Notebook images  by default. These are all based on Python 3.6 and provide a predefined set of Python packages based on the use case (e.g. `pyspark` for Spark job processing, `tensorflow` for model training).

This documentation explains how to build and add new images and how to add Jupyter extensions to the images.

.Prerequisites

None

.Procedure

===== Creating Custom Images

To add a custom Jupyter Notebook image to your Open Data Hub Instance you will need to follow a structure similar to link:https://github.com/vpavlin/jupyter-notebooks[Jupyter Notebooks] which holds definitions of our currently provided Jupyter Images.

The way JupyterHub spawner recognizes a notebook image is by listing `ImageStreams` in the namespace and filtering by a name suffix `-notebook`. You can point the `ImageStream` to an existing image (in a container registry) or create it via `BuildConfig`.

You can see example of using a `BuildConfig` in link:https://github.com/vpavlin/jupyter-notebooks/blob/master/images-spark.json[Spark Notebook image resource].

Following `ImageStream` definition can be used as an example of adding your `custom-notebook` image to ODH.

```
{
    "kind": "ImageStream",
    "apiVersion": "image.openshift.io/v1",
    "metadata": {
        "name": "custom-notebook"
    },
    "spec": {
        "tags": [
            {
                "annotations": {
                    "openshift.io/imported-from": "my.registry.com/my-repository/my-custom-notebook-image"
                },
                "from": {
                    "kind": "DockerImage",
                    "name": "my.registry.com/my-repository/my-custom-notebook-image:latest"
                },
                "name": "latest",
                "referencePolicy": {
                    "type": "Source"
                }
            }
        ],
        "lookupPolicy": {
            "local": true
        }
    }
}
```

After creating a file (e.g. `custom-notebook.json`) you can add it by running 

```
oc apply -f custom-notebook.json
```   

command.

===== Adding Jupyter Notebook Extension

Jupyter Notebook Extensions are a convenient way to extend the functionality of Jupyter UI. There is plenty of existing extensions. You can find a documentation on how to write an extension at link:https://jupyter-notebook.readthedocs.io/en/stable/extending/index.html[Extending the Notebook].

Once you are happy with your extension you can add it to Open Data Hub by submitting a pull request against https://github.com/vpavlin/jupyter-notebooks[Jupyter Notebooks repository]. If you intend to add the extension to all ODH notebook images you need to install it into `minimal-notebook` similarly to how our link:https://github.com/vpavlin/jupyter-notebooks/blob/8212a30f7a47b1f34884c21266cb20530134c9ff/minimal-notebook/.s2i/bin/assemble#L145-L147[Publish extension is installed].

In case your extension is published on PyPi, you can follow the example of link:https://github.com/vpavlin/jupyter-notebooks/blob/8212a30f7a47b1f34884c21266cb20530134c9ff/scipy-notebook/.s2i/bin/assemble#L13[widgetsextension] as it is installed in `scipy-notebook`.

NOTE: We will move the `jupyter-notebooks` repository in the future under ODH organization (https://gitlab.com/opendatahub/opendatahub-operator/issues/104[see issue #104]).

Once your PR is merged, you should also request a rebuild of the notebook images by creating an issue on Open Data Hub operator repository to make sure our published notebook images get updated in Quay.io registry.


//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

.Additional resources

* link:https://jupyter-notebook.readthedocs.io/en/stable/extending/index.html[Extending the Notebook]
* link:https://github.com/jupyter-on-openshift/jupyterhub-quickstart[OpenShift compatible version of the JupyterHub application]
* link:https://github.com/vpavlin/jupyter-notebooks[OpenShift compatible S2I builder for basic notebook images]
* link:https://github.com/aicoe/jupyterhub-ocp-oauth[JupyterHub deployment using OpenShift OAuth authenticator]
